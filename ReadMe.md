# Security Policies for rogitlab-duo

Project has also custom Secret Detecion RulesSet in itself, fo which authetication has to happen. In Other Projects we need to specify a [rmeote location with auth](https://docs.gitlab.com/ee/user/application_security/sast/customize_rulesets.html#specify-a-private-remote-configuration).

We will do this directly in the Security policy.

For this I created a Project Access token with the Reporter Permission and read_repository scope. `Extended-Ruleset`
The value is stored as a Gorup Masked variable in `EXT_RULESET_SEC_POL` and can be used in other running jobs.

The Bot User for this token can be found [like this](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#bot-users-for-projects) and will be lsiuted in [the project member list](https://gitlab.com/roche/playground/gitlab-duo/group-security-policies/-/project_members).

For now it is `not set yet` until the next token rotation July 2024 and will be stored in the Group CiCD vairable `USER_EXT_RULESET_SEC_POL`.